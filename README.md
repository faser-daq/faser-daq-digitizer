# Digitizer DAQ

For the initial development of FASER digitizer development for the readout of the scintillator and calorimeter PMTs.

# Setup

## Network Routing
This software is intended to be used with the setup that has as a central hub a switch 
that connects to :
- CERN network
- PC
- VME crate

The first step is to set up the internal local network ip routing.  Start by mapping the 
MAC address of the interface board to a unique ip address using the [arp command](https://www.computerhope.com/unix/arp.htm)
```
arp -i eth0 -s 192.168.122.2 00:00:56:15:30:A2
```
Now, add an appropriate routing to specify the network on which the above ip address 
operates using the [route command](https://www.computerhope.com/unix/route.htm)
```
ip route add 192.168.122.2 dev eth0
```

If this has worked, then you should be able to ping the ip address of the interface board
```
ping 192.168.122.2
```
and see a positive response returned and the "L" LED light blink on the board.

## Software Setup
The directions here describe the basic setup, but the precise output when you run the code (because
it will have been developed) may be slightly different. The software is built using [cmake](https://cmake.org).  
Start by cloning the repository
```
[root@pcatbnl2 FASER]# git clone https://USERNAME:PASSWORD@gitlab.cern.ch/faser-daq/faser-daq-digitizer.git
Initialized empty Git repository in /root/FASER/faser-daq-digitizer/.git/
remote: Enumerating objects: 37, done.
remote: Counting objects: 100% (37/37), done.
remote: Compressing objects: 100% (35/35), done.
remote: Total 37 (delta 13), reused 0 (delta 0)
Unpacking objects: 100% (37/37), done.
```
Next create your build directory and configure the code
```
[root@pcatbnl2 FASER]# mkdir build
[root@pcatbnl2 FASER]# cd build/
[root@pcatbnl2 build]# ls
[root@pcatbnl2 build]# cmake ../faser-daq-digitizer/
-- The C compiler identification is GNU 4.4.7
-- The CXX compiler identification is GNU 4.4.7
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /root/FASER/build
```
Now compile the code
```
[root@pcatbnl2 build]# make
Scanning dependencies of target sis3153-core
[ 25%] Building CXX object CMakeFiles/sis3153-core.dir/sis3153-core/sis3153ETH_vme_class.cpp.o
[ 50%] Linking CXX static library libsis3153-core.a
[ 50%] Built target sis3153-core
Scanning dependencies of target fdaq-ReadBoardInfo
[ 75%] Building CXX object CMakeFiles/fdaq-ReadBoardInfo.dir/faser-daq/sis3153_test0.cpp.o
[100%] Linking CXX executable fdaq-ReadBoardInfo
[100%] Built target fdaq-ReadBoardInfo
```
Finally, install the code, which will make the executables accessible from anywhere
```
[root@pcatbnl2 build]# make install
[ 50%] Built target sis3153-core
[100%] Built target fdaq-ReadBoardInfo
Install the project...
-- Install configuration: ""
-- Installing: /usr/local/bin/libsis3153-core.a
-- Installing: /usr/local/include/sis3153ETH_vme_class.h
-- Installing: /usr/local/bin/fdaq-ReadBoardInfo
```
You are now ready to run any of the executables which have been written and included in 
the CMakeLists.txt file for compilation into executables.

# DAQ Routines
These are the available routines that can be one once the setup is configured

## fdaq-ReadBoardInfo 
This just checks the communication ability by querying a few registers within the interface
board
```
[root@pcatbnl2 FASER]# fdaq-ReadBoardInfo 
sis3153eth_access_test
===================================================
Checking basic communication with interface board :
===================================================
get_vmeopen_messages = sis3153eth UDP port is open , nof_found_devices 1 
Control Status : 
udp_sis3153_register_read: addr = 0x00000000    data = 0x00000000    return_code = 0x00000111 
ModuleID and Firmware Version : 
udp_sis3153_register_read: addr = 0x00000001    data = 0x00000000    return_code = 0x00000111 
Serial Number : 
udp_sis3153_register_read: addr = 0x00000002    data = 0x00000000    return_code = 0x00000111 
Lemo I/O Register : 
udp_sis3153_register_read: addr = 0x00000003    data = 0x00000000    return_code = 0x00000111 
```
